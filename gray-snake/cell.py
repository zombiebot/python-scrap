#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

class Cell():
    def __init__(self, coor, color, border):
        self.x, self.y = coor
        self.defaultColor = color
        self.defaultBorder = border
        self.clear()
    def clear(self):
        self.status = 'FREE';
        self.color = self.defaultColor
        self.border = self.defaultBorder
        self.objRef = None
    def placeFood(self, foodObj):
        self.status = 'FOOD';
        self.color = foodObj.color
        self.border = foodObj.border
        self.objRef = foodObj
    def placeSnake(self, snakeObj):
        self.status = 'SNAKE';
        self.color = snakeObj.color
        self.border = snakeObj.border    
